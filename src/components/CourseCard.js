// import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function CourseCard({courseProp}) {

  // useState hook:
  // A hook in React is a kind of tool. The useState hook allows creation and manipulation of states
  // States are a way for React to keep track of any value and associate it with a component
  // When a state changes, React re-renders ONLY the specific component or part of the component that changed (and not the entire page or components whose states have not changed)

  // Syntax:
  // const [state, setState] = useState(default state)

  // state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed

  // state setter: State setters are the ONLY way to change a state's value. By convention, they are named after the state.

  // default state: The state's initial value on component mount
  // Before a component mounts, a state actually defaults to undefined, THEN is changed to its default state

  // Array destructuring to get the state and the setter
  // const [count, setCount] = useState(0)

  let { name, description, price, _id } = courseProp;

  /*
    ACTIVITY:
    Create a state hook that will represent the number of available seats for each course

    It should default to 10, and decrement by 1 each time a student enrolls

    Add a condition that will show an alert that no more seats are available if the seats state is 0. 
  */

  // const [seats, setSeat] = useState(10);

  // refactor the enroll function and instead use "useEffect" hooks
  /*function enroll() {
    if (count !== 10) {
      setCount(count + 1);
      setSeat(seats - 1);
    }  
  }*/
  // Apply the use effect hook
  // useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)

  // Syntax: 
    // useEffect(function, [dependencies])

  /*useEffect(() => {
    if (seats === 0) {
      alert("No more seats available!")
    }
  },[count, seats]);*/

  // alert("Maximum Enrollees reached");

  return (
    <Card className = "mb-3 p-3">
      <Card.Body className="card-purple">
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>&#8369; {price}</Card.Text>
        <Button as={Link} to={`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  )
};