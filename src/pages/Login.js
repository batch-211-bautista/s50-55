import { Form, Button } from 'react-bootstrap';
// Complete (3) Hooks of React
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	// Allows us to consume the User Context/Data and its properties for validation
	const { user, setUser } = useContext(UserContext);

	function loginUser(e) {

	    e.preventDefault();

	    fetch('http://localhost:4000/users/login', {
	    	method: 'POST',
	    	headers: {
	    		'Content-Type': 'application/json'
	    	},
	    	body: JSON.stringify({
	    		email: email,
	    		password: password
	    	})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);
	    	if (typeof data.access !== "undefined") {
	    		localStorage.setItem('token', data.access)
	    		retrieveUserDetails(data.access)

	    		Swal.fire({
	    			title: "Login Successful!",
	    			icon: "success",
	    			text: "Welcome to Booking App of 211!"
	    		})
	    	} else {
	    		Swal.fire({
	    			title: "Authentication Failed!",
	    			icon: "error",
	    			text: "Check your credentials!"
	    		})
	    	}
	    });

	    const retrieveUserDetails = (token) => {

	    	fetch('http://localhost:4000/users/details', {
	    		headers: {
	    			Authorization: `Bearer ${token}`
	    		}
	    	})
	    	.then(res => res.json())
	    	.then(data => {
	    		console.log(data);

	    		setUser({
	    			id: data._id,
	    			isAdmin: data.isAdmin
	    		})
	    	})

	    }

	    // set the email of the authenticated user in the local storage
	    /*
			Syntax:
				localStorage.setItem("propertyName", value);
	    */

	    // localStorage.setItem("email", email);

	    // Set the global user state to have properties obtained from local storage.
	    // This will pass the data to the UserContext which is ready to use from all different endpoints/pages
	    // setUser({
	    // 	email: localStorage.getItem("email")
	    // });

	    setEmail("");
	    setPassword("");

	    // alert("You are now logged in")
  	}

	useEffect(() => {

		if (email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	},[email, password]);

	return (
		// Conditional Rendering
		// LOGIC - if there is a user logged-in in the web application, endpoint "/login" or "/login" should not be accessible. The user should be navigated to courses tab instead.

		(user.id !== null)
		?
		<Navigate to="/courses" />
		// ElSE - if the localStorage is empty, the user is allowed to access the login page.
		:
		<>
			<h1>Login</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group className="mb-3" controlId="email">
					<Form.Label>Email address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value={email} 
						onChange={e => setEmail(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password" 
						value={password} 
						onChange={e => setPassword(e.target.value)}
					/>
				</Form.Group>

				{ isActive ?
					<Button variant="success" type="submit">
						Login
					</Button>
					:
					<Button variant="secondary" type="submit" disabled>
						Login
					</Button>
				}
			</Form>
		</>
	);
}