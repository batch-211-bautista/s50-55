import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout() {
	
	// Consume the UserContext Object and destructure it to access the user state and unsetUser function from our provider.
	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the localStorage or user's information.
	unsetUser();

	useEffect(() => {
		// Set the user state back to its original value
		setUser({id: null});
	});

	// localStorage.clear();

	return (
		<Navigate to="/login"/>
	)
}