// import coursesData from '../data/courses';
import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	// console.log(coursesData);

	// Using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	// props are a way to pass any valid JS data from parent component to child component.

	// map returns an array, which we can display in the page via the component functions's return statement

	// You can pass as many props as you want. Even functions can be passed as props.

	const [courses, setCourses] = useState([]);

	useEffect(() => {

		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			const courseArr = (data.map(course => {
				// console.log(course);
				return (
					<CourseCard courseProp={course} key={course._id}/>
				)
			}));
			setCourses(courseArr);
		})

	},[courses]);

	return (
		<>
			{courses}
		</>
	)
}