import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CourseCard from '../components/CourseCard';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export default function Home() {

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere!",
		destination: "/courses",
		label: "Enroll now!"
	}

	return (
		<>
			<Banner bannerProp={data} />
			<Highlights/>
		</>
	)
};