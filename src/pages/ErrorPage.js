import Banner from '../components/Banner';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { BiErrorCircle } from 'react-icons/bi';

export default function ErrorPage() {

	const data = {
		title: "404 - Not found",
		content: "The page you are looking cannot be found",
		destination: "/",
		label: "Back Home"
	}

	return (
		// "prop" name is up to the developer
		<>
			<Banner bannerProp={data} />
		</>
	)
}